package org.amdatu.web.http2;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.http.jetty.ConnectorFactory;
import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.util.ssl.SslContextFactory;

@Component
public class Http2ConnectorFactory implements ConnectorFactory {

    @Override
    public Connector createConnector(Server server) {
        HttpConfiguration httpConfiguration = new HttpConfiguration();
        httpConfiguration.setSecurePort(8443);
        httpConfiguration.setSecureScheme("https");
        httpConfiguration.setOutputBufferSize(32768);
        httpConfiguration.setRequestHeaderSize(8192);
        httpConfiguration.setResponseHeaderSize(8192);

        httpConfiguration.addCustomizer(new SecureRequestCustomizer());

        HTTP2ServerConnectionFactory connFactory = new HTTP2ServerConnectionFactory(httpConfiguration );

        ALPNServerConnectionFactory alpnServerConnectionFactory = new ALPNServerConnectionFactory("h2", "h2-17", "h2-16", "h2-15", "h2-14");

        SslContextFactory sslContextFactory = new SslContextFactory();
        configureSslContextFactory(sslContextFactory);

        ServerConnector serverConnector = new ServerConnector(server,
            1,  // config.getAcceptors(),
            1,  // config.getSelectors(),
            new SslConnectionFactory(sslContextFactory, "alpn"),
            alpnServerConnectionFactory, connFactory);

        serverConnector.setPort(8443);
        serverConnector.setHost("localhost");
        return serverConnector;
    }

    private void configureSslContextFactory(final SslContextFactory connector) {
        String keyStorePath = System.getProperty("keyStorePath");
        String keyStorePassword =  System.getProperty("keyStorePassword");
        String certAlias =  System.getProperty("certAlias");

        connector.setKeyStorePath(keyStorePath);
        connector.setKeyStorePassword(keyStorePassword);
        connector.setCertAlias(certAlias);

        connector.setRenegotiationAllowed(true);
    }

}
