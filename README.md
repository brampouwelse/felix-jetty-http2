# Apache Felix Http Jetty HTTP 2

This is a bnd(tools) workspace demonstrating how http2 can be used together with Apache Felix Http Jetty.  

Running depends on a specific version OpenJDK 1.8.0_151

To use with another JDK version another apln boot version will be required see: 
https://www.eclipse.org/jetty/documentation/9.3.25.v20180904/alpn-chapter.html#alpn-versions 


## Running 

**Bndtools**  

Use the run.bndrun in the run project 

**commandline** 

```
./gradlew clean jar export
java -Xbootclasspath/p:run/alpn-boot-8.1.11.v20170118.jar -jar run/generated/distributions/executable/run.jar
```

**Result**

Once started the Apache Felix Webconsole is available at https://localhost:8443/system/console


## Additional Runtime requirements  

The `org.amdatu.web.http2` bundle that's wrapping the http2 Jetty dependencies and provides a HTTP2 connector.   

And for ALPN to work the `alpn-boot-8.1.11.v20170118.jar` needs to be added to the boot classpath, then the `org.eclipse.jetty.osgi.alpn.fragment` bundle is will add the alpn package to the system bundle exports. 


## Issues 

The Jetty `PreEncodedHttpField` is obtaining available `org.eclipse.jetty.http.HttpFieldPreEncoder` classes using the ServiceLoader once in a static initializer. 
The current implementation works, by providing the `org.eclipse.jetty.http` from the bundle combined with a  `org.amdatu.web.http2/resources/META-INF/services/org.eclipse.jetty.http.HttpFieldPreEncoder` service loader config.    

But this is fragile as there is no guarantee this `org.eclipse.jetty.http` package will be loaded from this bundle instead of the  Felix Http bundle.
Installing the The `org.amdatu.web.http2` bundle  in a running framework won't work because of this.  
 